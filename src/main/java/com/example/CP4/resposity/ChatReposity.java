package com.example.CP4.resposity;


import com.example.CP4.model.Chat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatReposity extends CrudRepository<Chat, Integer> {
    List<Chat> findAll();
}
