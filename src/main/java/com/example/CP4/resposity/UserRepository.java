package com.example.CP4.resposity;



import com.example.CP4.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    Optional<User> findByUsername(String name);
    Boolean existsByUsernameAndPass(String name, String pass);
    Boolean existsByEmail(String email);
    Boolean existsByUsername(String username);
    List<User> findAll();
}
