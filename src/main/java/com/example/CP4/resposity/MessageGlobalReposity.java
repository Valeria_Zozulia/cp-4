package com.example.CP4.resposity;


import com.example.CP4.model.Chat;
import com.example.CP4.model.MessageGlobal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageGlobalReposity extends CrudRepository<MessageGlobal, Integer> {

   // List<MessageGlobal> findAllByto_whore(Chat chat);
    List<MessageGlobal> findAllByToWhore(Chat toWhore);
}
