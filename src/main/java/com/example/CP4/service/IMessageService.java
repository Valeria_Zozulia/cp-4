package com.example.messagingstompwebsocket.service;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

public interface IMessageService {
    void addUserCommand(WebSocketSession session, TextMessage message);
}
