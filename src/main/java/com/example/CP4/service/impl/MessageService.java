package com.example.CP4.service.impl;

import com.example.CP4.dto.*;

import com.example.CP4.model.MessageGlobal;
import com.example.CP4.model.User;
import com.example.CP4.resposity.ChatReposity;
import com.example.CP4.resposity.MessageGlobalReposity;
import com.example.CP4.resposity.MessagePrivateReposity;
import com.example.CP4.resposity.UserRepository;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MessageService implements com.example.messagingstompwebsocket.service.IMessageService {

    private final Gson gson = new Gson();
    private Map<Long,WebSocketSession> sessions = new HashMap<>();

    private static SimpleDateFormat format = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);

    private static User user;

    @Autowired
    private ChatReposity chatReposity;

    @Autowired
    private MessageGlobalReposity messageGlobalReposity;

    @Autowired
    private MessagePrivateReposity messagePrivateReposity;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void addUserCommand(WebSocketSession session, TextMessage message){

        Message messageService = gson.fromJson(message.getPayload(), Message.class);
//        sessions.putIfAbsent(Long.valueOf(messageService.getData().getUserId()),session);
        log.info("Data "+messageService.getData());
        log.info(messageService.getCommand().toString());
        switch (messageService.getCommand()) {
            case SENDMESSAGE: {
                format.setTimeZone(TimeZone.getTimeZone("UTC"));
                try {
                    messageGlobalReposity.save(new MessageGlobal(0,
                            format.format(new Date()),
                            user,
                            chatReposity.findById(Integer.valueOf(messageService.getData().getRoomId())).get(),
                            messageService.getData().getMessage()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case CURRENT_USER: {
                try {
                    user = userRepository.findById(2).get();
                            //Integer.parseInt(messageService.getData().getUserId())).get();
                    session.sendMessage(
                            new TextMessage("{\"command\":\"CURRENT_USER\", \"data\":"
                                    + gson.toJson(new UserToken(
                                    String.valueOf(user.getId_user()),
                                    user.getUsername(),
                                    user.getEmail(),
                                    user.getUsername())) + "}"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
            case JOINROOM: {
                try {
                    List<MessageGlobal> messages = messageGlobalReposity.findAllByToWhore(chatReposity.findById(Integer.valueOf(messageService.getData().getRoomId())).get());

                    ArrayList<MessageJson> messageJsons = (ArrayList<MessageJson>) messages.stream()
                            .map(e -> new MessageJson(e.getText(),
                                    e.getFromWhore().getUsername(),
                                    e.getTime()))
                            .collect(Collectors.toList());
                    session.sendMessage(
                            new TextMessage("{\"command\":\"JOINROOM\", \"data\":"
                                    + gson.toJson(messageJsons)
                                    + "}"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case ROOMS: {
                try {
                    ArrayList<RoomsJson> chatPublic = (ArrayList<RoomsJson>) chatReposity.findAll().stream().map(
                            e -> new RoomsJson(
                                    false,
                                    e.getId_chat(),
                                    e.getNameChat(),
                                    null,
                                    null,
                                    null,
                                    null)
                    ).collect(Collectors.toList());
                    session.sendMessage(
                            new TextMessage("{\"command\":\"ROOMS\", \"data\":"
                                    + gson.toJson(chatPublic)
                                    + "}"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case USERS: {

                List<User> users = userRepository.findAll();
//                ArrayList<RoomsJson> chatPublic = (ArrayList<RoomsJson>) users
//                        .parallelStream()
//                        .map(e->new RoomsJson(true,
//                                e.getId_user(),
//                                e.getUsername(),
//                                null,
//                                null,
//                                null,
//                                null)).collect(Collectors.toList());
//
//                try {
//                    session.sendMessage(
//                            new TextMessage("{\"command\":\"USERS\", \"data\":"
//                                    + gson.toJson(chatPublic)
//                                    + "}"));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                List<PrivateChat> privateChats = users
                        .parallelStream()
                        .map(e->new PrivateChat(e.getId_user(),e.getUsername(),e.getEmail())).collect(Collectors.toList());
                try {
                    session.sendMessage(
                            new TextMessage("{\"command\":\"USERS\", \"data\":"
                                    + gson.toJson(privateChats)
                                    + "}"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
            case LOGIN: {
                break;
            }
            case LOGOUT: {
                break;
            }
            case CREATECHAT: {
                break;
            }
            case REMOVECHAT: {
                break;
            }
            case REGISTERUSER: {
                break;
            }
            case STARTGAME: {
                break;
            }
            case STOPGAME: {
                break;
            }
        }


    }

}