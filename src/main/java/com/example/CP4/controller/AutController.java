package com.example.CP4.controller;



import com.example.CP4.config.JwtUtils;

import com.example.CP4.model.User;
import com.example.CP4.payload.request.LoginRequest;
import com.example.CP4.payload.request.SignupRequest;
import com.example.CP4.payload.response.JwtResponse;
import com.example.CP4.payload.response.MessageResponse;
import com.example.CP4.resposity.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@RestController
@CrossOrigin
@Slf4j
public class AutController {

    @Autowired
    private UserRepository userRepository;

//    @Autowired
//    private PasswordEncoder encoder;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;

//    @Autowired
//    private AuthenticationManager authenticationManager;

    @PostMapping("/registration")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getPassword(),
                signUpRequest.getEmail());

        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

//        Authentication authentication = authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
//
//        SecurityContextHolder.getContext().setAuthentication(authentication);
        User user;
        if( userRepository.existsByUsernameAndPass(loginRequest.getEmail(), loginRequest.getPassword())){
          user =  userRepository.findByUsername(loginRequest.getEmail()).get();
        }else {
            return ResponseEntity.badRequest().body("нету такого");
        }
        String jwt = jwtUtils.generateJwtToken(user);



        return ResponseEntity.ok(new JwtResponse(jwt,
                user.getId_user(),
                user.getUsername(),
                user.getEmail()));
    }

    @PostMapping("/login/google")
    public String getLoginInfo(Model model, OAuth2AuthenticationToken authentication) {
        OAuth2AuthorizedClient client = authorizedClientService
                .loadAuthorizedClient(authentication.getAuthorizedClientRegistrationId(), authentication.getName());

        return "login/google";
    }

}

