package com.example.CP4.config;


import com.example.CP4.WebSocketHandlerImpl;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.socket.config.annotation.*;

@Configuration
@EnableWebSocket
@CrossOrigin
public class WebSocketConfig implements WebSocketConfigurer {

	private final WebSocketHandlerImpl handler;

	public WebSocketConfig(WebSocketHandlerImpl handler){
		this.handler = handler;
	}


	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
		webSocketHandlerRegistry.addHandler(handler,"/ws").setAllowedOrigins("*");
	}

}