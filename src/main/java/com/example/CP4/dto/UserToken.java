package com.example.CP4.dto;

public class UserToken {
    private String id;

    private String userName;

    private String email;

    private String name;

    public UserToken(String id, String userName, String email, String name) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

}

