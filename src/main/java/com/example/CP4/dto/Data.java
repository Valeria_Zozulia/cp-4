package com.example.CP4.dto;

public class Data {

    private String message;
    private String userId;
    private String roomId;
    private String date;

    public String getMessage() {
        return message;
    }

    public String getUserId() {
        return userId;
    }

    public String getRoomId() {
        return roomId;
    }

    public String getDate() {
        return date;
    }
}