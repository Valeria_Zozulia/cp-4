package com.example.CP4.dto;

public class Message {
    private Command command;
    private Data data;

    public Command getCommand() {
        return command;
    }
    
    public Data getData() {
        return data;
    }

}
