package com.example.CP4.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

//  {
//          "is_user": false,
//          "is_private": true,
//          "_id": "5cb676b69f22822f33593a77",
//          "name": "My Private Room",
//          "description": "My Private Room",
//          "users": [],
//          "messages": [],
//          "created_at": "2019-04-17T00:43:34.114Z",
//          "updated_at": "2019-04-17T00:43:34.115Z",
//          "__v": 0
//          },
@Getter
@Setter
public class PrivateChat {
    private boolean is_user;
    private boolean is_private;
    private int _id;
    private List users;
    private List messages;
    private String name;
    private String description;
    private int __v;
    private String created_at;
    private String updated_at;

    public PrivateChat() {
    }

    public PrivateChat( int _id, String name, String description) {
        this.is_user = false;
        this.is_private = true;
        this._id = _id;
        this.users = null;
        this.messages = null;
        this.name = name;
        this.description = description;
        this.__v = 0;
        this.created_at=null;
        this.updated_at=null;
    }
}
