package com.example.CP4.dto;

public enum Command {
    SENDMESSAGE,
    LOGIN,
    LOGOUT,
    CREATECHAT,
    REMOVECHAT,
    REGISTERUSER,
    STARTGAME,
    STOPGAME,
    CURRENT_USER,
    USERS,
    JOINROOM,
    ROOMS
}
