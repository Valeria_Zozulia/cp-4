package com.example.CP4;

import com.example.messagingstompwebsocket.service.IMessageService;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Component
public class WebSocketHandlerImpl extends TextWebSocketHandler {

    private final IMessageService service;

    public WebSocketHandlerImpl(IMessageService service) {
        super();
        this.service = service;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session)throws Exception{
            super.afterConnectionEstablished(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        service.addUserCommand(session,message);
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        super.handleTransportError(session,exception);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session,status);
    }

}
