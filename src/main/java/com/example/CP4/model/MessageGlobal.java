package com.example.CP4.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "messageGlobal")
@Entity
public class MessageGlobal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_message_global;
    private String time;
    @ManyToOne
    @JoinColumn(name="from_whore")
    private User fromWhore;
    @ManyToOne
    @JoinColumn(name="to_whore")
    private Chat toWhore;
    private String text;
}
