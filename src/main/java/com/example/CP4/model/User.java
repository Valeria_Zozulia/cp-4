package com.example.CP4.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "user")
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_user;

    private String username;

    private String pass;

    private String email;

    private Boolean isOnline;

    private String avatar;

    private boolean action;

    public User(String username, String pass, String email) {
        this.username = username;
        this.pass = pass;
        this.email = email;
        this.isOnline = true;
        this.action = false;
    }
}
