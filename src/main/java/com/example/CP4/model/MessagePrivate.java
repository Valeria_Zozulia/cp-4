package com.example.CP4.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "messagePrivate")
@Entity
public class MessagePrivate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_message_global;
    private Date time;
    @ManyToOne
    @JoinColumn(name="from_whore")
    private User from_whore;
    @ManyToOne
    @JoinColumn(name="to_whore")
    private User to_whore;
    private String text;
}
