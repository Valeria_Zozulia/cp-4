

function connect() {
    WS = new WebSocket("ws://localhost:8080/ws?token=dsadasdasdasdasdasdasdasdasdasd");

    WS.onopen=function (e) {
    };
    WS.onmessage = function (event) {
        showGreding(event.data);
    };
    WS.onclose = function (event) {
        if(event.wasClean){
            alert("соединение закрыто");
        }
    };
    WS.onerror = function (error) {
        alert(error);
    }
}

function disconnect() {
    if(WS!=null){
        WS.close();
    }
}

function sendName() {
    var data = JSON.stringify({
        'name' : $("#name").val()
    });
    WS.send(data);
}

function showGreding(message) {
    $("#greetings").append(message);
}

$(function () {
    $("form").on('submit',function (e) {
        e.preventDefault();
    });
    $("#connect").click(function () {
        connect();
    });
    $("#disconnect").click(function () {
        disconnect();
    });
    $("#send").click(function () {
        sendName();
    });
});
